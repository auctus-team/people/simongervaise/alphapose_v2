import numpy as np
import matplotlib.pyplot as plt

data = open("gaze_detection.txt", "r")
result = open("gaze_result.txt", "w")
frequence = open("gaze_frequency.txt", "r")

temps = [0]

donnée = []

actual_gaze = 0
last_gaze = 0
time = 0

temps_droite = 0
temps_gauche = 0
temps_devant = 0

data.seek(0)

for line in frequence.readlines():
	frequence = float(line)
	print("fréquence :", frequence)

for line in data.readlines():
	last_gaze = actual_gaze
	time += frequence
	if line == 'regard tout droit\n':
		temps_devant += frequence
		actual_gaze = 'droit'
		donnée.append(1)
	elif line == 'regard à gauche\n':
		temps_gauche += frequence
		actual_gaze = 'gauche'
		donnée.append(0)
	elif line == 'regard à droite\n':
		temps_droite += frequence
		actual_gaze = 'droite'
		donnée.append(2)
	else:
		donnée.append(None)

	if actual_gaze != last_gaze and last_gaze !=0:
		result.write(last_gaze)
		result.write( " : ")
		result.write(str(time))
		result.write("\n")
		time = 0

while len(temps) < len(donnée):
	temps.append(temps[-1] + frequence)

print("Temps passé à gauche :", temps_gauche, "s")
print("Temps passé à droite :", temps_droite, "s")
print("Temps passé devant :", temps_devant, "s")


y = ["à gauche", "devant", "à droite"]
default_y_ticks = range(len(y))
plt.plot(temps, donnée)
plt.yticks(default_y_ticks, y)
plt.show()

